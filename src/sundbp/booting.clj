(ns sundbp.booting
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [boot.task.built-in :as bi]
            [boot.util :as u]
            [boot.core :as core]
            [boot.git :as git]
            [boot.pod :as pod]

            [sundbp.booting.util :refer (verify!)]
            [sundbp.booting.pipeline :as pipeline]
            [sundbp.booting.git :as bgit]))

;; Some things shamelessly stolen from Bootlaces
;; https://github.com/adzerk-oss/bootlaces/blob/master/src/adzerk/bootlaces.clj
(def ^:private +last-commit+
  (try (git/last-commit) (catch Throwable _)))
(defn- get-creds []
  (mapv #(System/getenv %) ["REPO_USER" "REPO_PASS"]))

(defn collect-repo-credentials
  [repo]
  (let [[user pass] (get-creds), repo-creds (atom {})]
    (if (and user pass)
      (swap! repo-creds assoc :username user :password pass)
      (do (println "REPO_USER and REPO_PASS were not set; please enter your deploy repo credentials for repo:" repo)
          (print "Username: ")
          (#(swap! repo-creds assoc :username %) (read-line))
          (print "Password: ")
          (#(swap! repo-creds assoc :password %)
           (apply str (.readPassword (System/console))))))
    (let [repos (->> (core/get-env :repositories)
                     (map (fn [[r opts]]
                            (if (= r repo)
                              [r (merge @repo-creds opts)]
                              [r opts]))))]
      (core/set-env! :repositories repos))))

(core/deftask deploy-release
  [f file PATH str "The jar file to deploy"
   p pom  PATH str "The pom file to use"
   r repo REPO str "The name of the repo to deploy to"]
  (collect-repo-credentials repo)
  (bi/push
   :file           file
   :pom            pom
   :tag            (boolean +last-commit+)
   :gpg-sign       true
   :ensure-release true
   :repo           repo))

(def ^:private release-deps '[[grimradical/clj-semver "0.3.0"]])

(defn- pod-with-deps [deps]
  (pod/make-pod (update (core/get-env) :dependencies into deps)))

(def ^:private release-pod
  (pod-with-deps release-deps))

(defn- update-build-boot-version! [old-version new-version]
  (let [^java.io.File build-boot (io/file "build.boot")]
    (when-not (.exists build-boot)
      (u/exit-error "Could not find the build.boot file!"))
    (let [old-contents (slurp build-boot)
          new-contents (.replaceFirst old-contents old-version new-version)]
      (when (= old-contents new-contents)
        (u/exit-error (format "Could not update the version (from %s to %s) in build.boot file!"
                              old-version new-version)))
      (spit build-boot new-contents))))

(defn- get-release-version [version bump]
  (pod/with-call-in release-pod
    (sundbp.booting.release/next-release-version ~bump ~version)))

(defn- get-snapshot-version [version bump]
  (pod/with-call-in release-pod
    (sundbp.booting.release/next-snapshot-version ~bump ~version)))

(defn- is-pre-release? [version]
  (pod/with-call-in release-pod
    (sundbp.booting.release/is-pre-release? ~version)))

(defn- get-pom-options []
  (-> #'bi/pom meta :task-options))

(defn- get-current-version []
  (-> (get-pom-options) :version))

(core/deftask bump-version
  "Bump project version by modifying the `build.boot` file. The version must be
  specified as the first thing in the file, e.g.

    `(def version \"0.1.0-SNAPSHOT\")`

  Uses semantic versioning if not overridden."
  [v version VER str  "Version to set, e.g. \"1.0.0\".g. snapshot) version."
   b bump    TYP kw   "Version bump to apply. Can be one of: major, minor or patch."
   r release     bool "Specify when performing a release. Will remove pre-release and build modifiers."]
  (core/with-pass-thru _
    (let [current-version (get-current-version)]
      (verify! (not (and version bump))
               (str "Please specify either a version to set or a bump "
                    "to apply to the current version, got: %s and %s!") version bump)
      (verify! (or version (and release (is-pre-release? current-version))
                   (#{:major :minor :patch} bump))
               "Bump must be one of: major, minor or patch. Got: %s!" bump)
      (let [new-version (or version (if release
                                      (get-release-version current-version bump)
                                      (get-snapshot-version current-version bump)))]
        (u/info "Bumping to version %s (was %s)...\n" new-version current-version)
        (update-build-boot-version! current-version new-version)
        (bgit/add-and-commit! (format "Version %s" new-version))))))

(core/deftask git-push
  [t tags bool "Push tags?"]
  (core/with-pass-thru _
    (bgit/push-with-tags!)))

(defmacro defreleasetask [name & commands]
  (core/template
    (boot.core/deftask ~name [v version      VER str "Release version"
                              b bump         BUM kw  "Release version bump"
                              V next-version VER str "Version after release"
                              B next-bump    BUM kw  "Bump after release"]
      (require 'sundbp.booting.pipeline)
      (sundbp.booting.pipeline/pipelines*
        (concat
          [(sundbp.booting.pipeline/compile
             `(sundbp.booting/bump-version :version ~version, :bump ~bump, :release true))]
          (mapv sundbp.booting.pipeline/compile (quote ~commands))
          [(sundbp.booting.pipeline/compile
             `(comp (sundbp.booting/bump-version :version ~next-version, :bump ~(or next-bump :patch), :release false)
                    (sundbp.booting/git-push :tags true)))])))))

(defn- manifest [version]
  (let [now (pr-str (java.util.Date.))
        build-version (or (System/getenv "BUILD_VERSION")
                          (.substring now (inc (.indexOf now "\"")) (.lastIndexOf now ".")))
        build-number (or (System/getenv "BUILD_NUMBER") build-version)]
    {"Implementation-Version" (str version "." build-version)
     "Build-Version" build-version
     "Build-Number" build-number}))

(core/deftask standalone
  "Creates a standalone uberjar with a manifest."
  [m main NS sym "Main namespace to AOT"]
  (let [{:keys [project version]} (get-pom-options)
        classifier "standalone"]
    (comp (bi/aot :namespace #{main})
          (bi/pom :classifier classifier)
          (bi/uber)
          (bi/jar :manifest (manifest version)
                  :main main
                  :file (str (name project) "-" version "-" classifier ".jar")))))

