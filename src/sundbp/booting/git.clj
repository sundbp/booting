(ns sundbp.booting.git
  (:require [boot.pod :as pod]))

;; boot-worker depends on [clj-jgit "0.8.0"]
(defn add-and-commit! [message]
  (pod/with-eval-worker
    (require '[clj-jgit.porcelain :as jgit]
             '[boot.jgit :as boot.jgit])
    (boot.jgit/with-repo
      (jgit/git-add-and-commit repo ~message))))

(defn push-with-tags! []
  (pod/with-eval-worker
    (require '[clojure.java.shell :as shell]
             '[clojure.string :as string]
             '[boot.util :as u])
    (let [{:keys [out err exit]} (shell/sh "git" "push" "--follow-tags")]
      (when-not (= 0 exit)
        (u/fail "Could not push to remote: %s!" (string/join ", " [out err]))))))
