(ns sundbp.booting.pipeline
  (:refer-clojure :exclude (compile))
  (:require [clojure.string :as string]
            [boot.util :as u]
            [boot.parallel :as par]
            [sundbp.booting.util :refer (verify!)]))

(defn- task-info [task]
  (let [task-var (resolve task)]
    (verify! task-var "Could not resolve task: %s!" task)
    (let [opts (:argspec (meta task-var))
          as-map #(->> (partition 2 %) (map vec) (into {}))
          map->opts #(select-keys % [:id :long-opt :required])]
      (->> (map as-map opts)
           (group-by :id)
           (map (fn [[k v]] [k (map->opts (first v))]))
           (into {})))))

(defn- arg->string [arg-format arg]
  (when arg-format
    (verify! (not (re-find #"[=:]" arg-format))
             "Don't know how to print complex arguments yet. Got: %s -> %s!" arg-format arg))
  (when arg
    (if (instance? clojure.lang.Named arg)
      (name arg)
      arg)))

(defn- fail-parsing! [task args currently-parsing arg parsed-arg->opts]
  (verify! false "Could not parse (%s %s), failed on %s (%s). Parsed: %s!"
           task (pr-str args) arg currently-parsing parsed-arg->opts))

(defn- filter-vals [f m]
  (->> (for [[k v] m :when (f v)]
         [k v])
       (into {})))

(defn- collect-optargs [task task-opts args]
  (->> (reduce
         (fn [[current-opt arg->opts] a]
           (if-let [[long-opt required] current-opt]
             [nil (assoc arg->opts long-opt (arg->string required a))]
             (if-let [{:keys [required long-opt]} (get task-opts a)]
               [[long-opt required] arg->opts]
               (fail-parsing! task args current-opt a arg->opts))))
         [nil {}] args)
       (second)
       (filter-vals #(not (nil? %)))
       (mapcat (fn [[opt arg]]
                 (if (#{true false} arg)
                   [opt]
                   [opt arg])))))

(defn- compile-args [task args]
  (let [opts (task-info task)]
    (collect-optargs task opts args)))

(defn- compile-string [[start & more]]
  (->> (if (= (name start) "comp")
         (map compile-string more)
         [(string/join " " (concat [(str start)] (compile-args start more)))])
       (string/join " -- ")))

(defn compile [pipeline]
  (if (string? pipeline)
    pipeline
    (compile-string pipeline)))

(defn pipelines* [commands]
  (par/runcommands :commands commands :batches 1))

(defmacro pipelines
  "Run pipelines in separate boot cores. Pipeline commands can be expressed
  either as Clojure forms or as strings, e.g.:

    (pipelines
      (bump-version :release \"1.0.0\")
      (comp (pom) (jar) (push)))

  equivalent to:

    (pipelines
      \"bump-version --release 1.0.0\"
      \"pom -- jar -- push\")"
  [& commands]
  `(pipelines* (mapv compile ~commands)))
