(ns sundbp.booting.release
  (:require [clojure.string :as string]
            [clj-semver.core :as semver]))

(defn- bump-version [bump {:keys [major minor patch pre-release] :as v}]
  (case bump
    :major [(inc major) 0 0]
    :minor [major (inc minor) 0]
    :patch [major minor (inc patch)]
    [major minor patch]))

(defn next-release-version [bump v]
  (->> (semver/version v)
       (bump-version bump)
       (string/join ".")))

(defn next-snapshot-version [bump v]
  (str (next-release-version bump v) "-SNAPSHOT"))

(defn is-pre-release? [v]
  (let [{:keys [pre-release build]} (semver/version v)]
    (boolean (or pre-release build))))
