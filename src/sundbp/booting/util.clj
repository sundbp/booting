(ns sundbp.booting.util
  (:require [boot.util :as u]))

(defn verify! [condition msg & args]
  (when-not condition
    (u/exit-error
     (apply u/fail msg args))))
