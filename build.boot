(def project 'sundbp/booting)
(def version "0.1.9-SNAPSHOT")

(set-env! :resource-paths #{"src"}
          :dependencies '[[sundbp/booting "0.1.7" :scope "test"]])

(require '[sundbp.booting :as b])

(task-options!
  pom {:project     project
       :version     version
       :description "Tasks and utilities I use for my bood based projects."
       :url         "http://gitlab.com/sundbp/booting"
       :scm         {:url "http://gitlab.com/sundbp/booting"}
       :license     {"MIT" "https://opensource.org/licenses/MIT"}}
  b/deploy-release {:repo "clojars"})

(deftask build []
  (comp (pom) (jar) (install)))

(b/defreleasetask release
  (comp (pom) (jar) (b/deploy-release)))
